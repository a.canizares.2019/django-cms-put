from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Content

@csrf_exempt
def get_content(request, key):
    if request.method == "PUT":
        value = request.body.decode('utf-8')
        c = Content(key=key, value=value)
        c.save()
    try:
        respuesta = Content.objects.get(key=key).value
    except Content.DoesNotExist:
        respuesta = ("No existe contenido para esta clave " + key)
    return HttpResponse(respuesta)